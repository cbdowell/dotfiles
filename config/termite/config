# ---------------------------------------------------
#  _____              _ _
# |_   _|__ _ _ _ __ (_) |_ ___
#   | |/ -_) '_| '  \| |  _/ -_)
#   |_|\___|_| |_|_|_|_|\__\___|
#
# Author: Christopher Dowell <cbdowell@gmail.com>
# License: MIT
# ---------------------------------------------------

#
# /~\ _ _|_. _  _  _
# \_/|_) | |(_)| |_\
#   |

[options]

    allow_bold            = true
    audible_bell          = false
    clickable_url         = true
    dynamic_title         = true
    font                  = mplus Nerd Font 12
    mouse_autohide        = false
    resize_grip           = false
    urgent_on_bell        = true
    visible_bell          = false

    # fullscreen          = true
    # geometry            = 640x480
    # icon_name           = terminal
    # scroll_on_output    = false
    # scroll_on_keystroke = true


    # Length of the scrollback buffer, 0 disabled the scrollback buffer
    # and setting it to a negative value means "infinite scrollback"
    scrollback_lines = 10000
    search_wrap      = true


    # $BROWSER is used by default if set, with xdg-open as a fallback
    # browser = xdg-open


    # "system", "on" or "off"
    cursor_blink = system


    # "block", "underline" or "ibeam"
    cursor_shape = block


    # Hide links that are no longer valid in url select overlay mode
    # filter_unmatched_urls = true


    # Emit escape sequences for extra modified keys
    modify_other_keys = true


    # set size hints for the window
    # size_hints = false


    # "off", "left" or "right"
    # scrollbar = off


#
# ~|~|_  _  _ _  _
#  | | |(/_| | |(/_
#

[colors]


    # If both of these are unset, cursor falls back to the foreground color,
    # and cursor_foreground falls back to the background color.
    foreground = #ebdbb2
    foreground_bold = #ebdbb2


    # 20% background transparency (requires a compositor)
    background = rgba(40, 40, 40, 0.6)


    # If unset, will reverse foreground and background
    highlight = #d65d0e


    # Colors from color0 to color254 can be set
    # dark0 + gray
    color0 = #282828
    color8 = #928374


    # neutral_red + bright_red
    color1 = #cc241d
    color9 = #fb4934


    # neutral_green + bright_green
    color2 = #98971a
    color10 = #b8bb26


    # neutral_yellow + bright_yellow
    color3 = #d79921
    color11 = #fabd2f


    # neutral_blue + bright_blue
    color4 = #458588
    color12 = #83a598


    # neutral_purple + bright_purple
    color5 = #b16286
    color13 = #d3869b


    # neutral_aqua + bright_aqua
    color6 = #689d6a
    color14 = #8ec07c


    # light4 + light1
    color7 = #a89984
    color15 = #ebdbb2


#
# |_|. _ _|_ _
# | ||| | | _\
#

[hints]

    # font              = Monospace 12
    # foreground        = #dcdccc
    # background        = #3f3f3f
    # active_foreground = #e68080
    # active_background = #3f3f3f
    # padding           = 2
    # border            = #3f3f3f
    # border_width      = 0.5
    # roundness         = 2.0


# vim: ft=dosini cms=#%s

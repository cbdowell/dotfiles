function serve-this -d "Launch a webserver on 8000 serving the cwd"
    # python -m SimpleHTTPServer
    python3 -m http.server 8000
end

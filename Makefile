export DOTFILES_DIRS := $(CURDIR)
export RCRC := $(CURDIR)/rcrc


.PHONY: all
all:
	@ rcup -vf -d $(CURDIR)


up:
	@ rcup -vf $(targets)


fonts:
	@ scripts/install-iosevka
	@ scripts/install-mplus


include */Makefile
